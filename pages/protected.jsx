import React, { useContext } from "react";
import { useRouter } from "next/router";
import { getUserFromServerSession } from '../src/helpers/helper';
import { AuthContext } from "../src/context/AuthContext";

const ProtectedPage = () => {
    const router = useRouter();
    const { user } = useContext(AuthContext);

    const handleLogout = async (e) => {
        e.preventDefault();
        
        const response = await fetch("/api/logout", {
            method: "DELETE",
            headers: { "Content-Type": "application/json" }
        });

        if (response.ok) {
            return router.push("/signin");
        }
    };

    return (
        <div>
            <h1>Hello {user === undefined ? 'anonymous' : user?.token}</h1>
            <p>Secret things live here...</p>
            { user === undefined ? "" : <button onClick={handleLogout}>Logout</button>}
        </div>
    )
};

export const getServerSideProps = getUserFromServerSession({
    redirectToLogin: true,
});

export default ProtectedPage;