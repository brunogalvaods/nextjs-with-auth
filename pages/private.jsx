import React, { useContext, useState } from "react";
import { useRouter } from "next/router";
import { getUserFromServerSession } from '../src/helpers/helper';
import { AuthContext } from "../src/context/AuthContext";

const PrivatePage = (/*{user}*/) => {
    const router = useRouter();
    const { user } = useContext(AuthContext);
    const [consentsTransmissions, setConsentsTransmissions] = useState([]);

    const handleLogout = async (e) => {
        e.preventDefault();

        const response = await fetch("/api/logout", {
            method: "DELETE",
            headers: { "Content-Type": "application/json" }
        });

        if (response.ok) {
            return router.push("/signin");
        }
    };

    const handleProtected = (e) => {
        e.preventDefault();
        router.push("/protected");
    }

    const handleConsentsTransmissions = async (e) => {
        e.preventDefault();

        const response = await fetch("/api/consents/transmitions", {
            method: "GET",
            headers: { "Content-Type": "application/json" }
        });

        response.json().then((json) => {
            setConsentsTransmissions(json);
        });
    }

    return (
        <div>
            <h1>Hello {user?.token}</h1>
            <p>Secret things live here...</p>
            <button onClick={handleLogout}>Logout</button>
            <button onClick={handleProtected}>Protected</button>
            <button onClick={handleConsentsTransmissions}>Get consents transmissions</button>
            <select>
                {
                    consentsTransmissions?.map((consent) =>
                        <option key={consent.consentId} value={consent.consentId}>
                            {consent.consentId}
                        </option>)
                }
            </select>
        </div>
    )
};

export const getServerSideProps = getUserFromServerSession({
    redirectToLogin: true,
});

export default PrivatePage;