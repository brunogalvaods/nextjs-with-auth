import Head from 'next/head'
import Router from 'next/router';

export default function Home() {
  
  const handleSignin = (e) => {
    e.preventDefault();
    Router.push('/signin');
  }

  return (
    <div>
      <Head>
        <title>Authentication Application Example</title>
        <meta name="description" content="Authentication Application Example" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <h1>Home Page</h1>
        <button onClick={handleSignin}>Go Signin!</button>
      </main>

      <footer>
      </footer>
    </div>
  )
}
