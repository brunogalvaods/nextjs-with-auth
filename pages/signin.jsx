import React, { useRef, useContext } from "react";
import { useRouter } from "next/router";
import { AuthContext } from '../src/context/AuthContext';
import { SigninForm } from "@/components/molecules/SigninForm";

const SignInPage = ({ env }) => {
    const router = useRouter();
    const usernameInput = useRef();
    const passwordInput = useRef();
    const { signIn } = useContext(AuthContext);

    const handleSubmit = async (e) => {
        e.preventDefault();

        const username = usernameInput.current.value;
        const password = passwordInput.current.value;

        await signIn(username, password);

        // const email = emailInput.current.value;
        // const password = passwordInput.current.value;

        // const response = await fetch("/api/sessions", {
        //     method: "POST",
        //     headers: { "Content-Type": "application/json" },
        //     body: JSON.stringify({ email, password })
        // });

        // if (response.ok) {
        //     return router.push("/private");
        // }
    };

    return (
        <SigninForm onSubmit={handleSubmit} username={usernameInput} password={passwordInput}></SigninForm>
    );
};

export default SignInPage;

export async function getServerSideProps() {
    console.log("The env variable is always availaible at server side: " + process.env.NEXT_PUBLIC_APP_ENV);
    return {
        props: {
            env: process.env.NEXT_PUBLIC_APP_ENV
        }
    }
}