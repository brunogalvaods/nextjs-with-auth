import { withIronSessionApiRoute } from "iron-session/next";
import axios from "axios";
import qs from "qs";

export default withIronSessionApiRoute(handleTransmissions,
  {
    cookieName: "MYSITECOOKIE",
    cookieOptions: {
      secure: process.env.NODE_ENV === "production" ? true : false
    },
    password: process.env.APPLICATION_SECRET
  }
);

async function handleTransmissions(req, res) {
  if (req.method === "GET") {

    const { token } = req.session.user;

    console.log("handleTransmissions: " + token);

    const config = {
      method: 'get',
      url: `http://apigateway.dev.scd.open-co.tech/api/management/consents/v1/consents/data/transmitions`,
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
        'Authorization': `Bearer ${token}`
      }
    };

    const response = await axios(config);

    return res.status(200).json(response.data);
  }

  return res.status(404).send("");
}