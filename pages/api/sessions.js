import { withIronSessionApiRoute } from "iron-session/next";

const VALID_EMAIL = "chris@decimal.fm";
const VALID_PASSWORD = "opensesame";

const VALID_EMAIL2 = "bruno.silva@open-co.com.br";
const VALID_PASSWORD2 = "opensesame";

export default withIronSessionApiRoute(handleLogin,
    {
        cookieName: "MYSITECOOKIE",
        cookieOptions: {
            secure: process.env.NODE_ENV === "production" ? true : false
        },
        password: process.env.APPLICATION_SECRET
    }
);

async function handleLogin(req, res) {
    if (req.method === "POST") {
        const { email, password } = req.body;

        if ((email === VALID_EMAIL && password === VALID_PASSWORD)
            || (email === VALID_EMAIL2 && password === VALID_PASSWORD2)) {
            
            req.session.user = {
                email: email
            };
            await req.session.save();
            return res.status(200).json({
                user: {
                    email: email
                }
            });
        }

        return res.status(403).send("");
    }

    return res.status(404).send("");
}