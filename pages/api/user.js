import { withIronSessionApiRoute } from "iron-session/next";

export default withIronSessionApiRoute(handleUser,
    {
        cookieName: "MYSITECOOKIE",
        cookieOptions: {
            secure: process.env.NODE_ENV === "production" ? true : false
        },
        password: process.env.APPLICATION_SECRET
    }
);

async function handleUser(req, res) {
    if (req.method === "GET") {
        const user = req.session.user;

        return res.status(200).json({
            user: user
        });
    }

    return res.status(404).send("");
}