import { withIronSessionApiRoute } from "iron-session/next";
import axios from "axios";
import qs from "qs";

export default withIronSessionApiRoute(handleLogin,
  {
    cookieName: "MYSITECOOKIE",
    cookieOptions: {
      secure: process.env.NODE_ENV === "production" ? true : false
    },
    password: process.env.APPLICATION_SECRET
  }
);

async function handleLogin(req, res) {
  if (req.method === "POST") {

    const { username, password } = req.body;

    const data = qs.stringify({
      'grant_type': 'password',
      'client_id': 'finansystech',//process.env.CLIENT_ID,
      'client_secret': '903d3047-70b3-4d7d-aaf5-3f64c612baad',//process.env.CLIENT_SECRET,
      'username': username,
      'password': password
    });

    const config = {
      method: 'post',
      url: `https://iam.dev.scd.open-co.tech/auth/realms/openco/protocol/openid-connect/token`,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      data: data
    };

    const response = await axios(config);

    req.session.user = {
      token: response.data.access_token
    };
    await req.session.save();

    return res.status(200).json({
      user: {
        token: response.data.access_token
      }
    });
  }

  return res.status(404).send("");
}