import { SigninForm } from "./styles";
import SigninFormComponentType from "./types";
import { Button, Input } from "@chakra-ui/react";
import { useState } from "react";

export const SigninFormComponent: SigninFormComponentType = ({ ...props }) => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const handleUsername = (e: any) => {
    setUsername(e.target.value);
  }

  const handlePassword = (e: any) => {
    setPassword(e.target.value);
  }

  return (
    <SigninForm {...props}>
      <div>
        <label>
          Username: <Input id="username" placeholder="username" value={props.username} onChange={handleUsername} />
        </label>
      </div>
      <div>
        <label>
          Password: <Input id="password" type={"password"} placeholder="password" value={props.password} onChange={handlePassword} />
        </label>
      </div>
      <div>
        <Button type="submit" colorScheme='blue' width='100%'>Sign in</Button>
      </div>
    </SigninForm>
  );
};

export default SigninFormComponent;
