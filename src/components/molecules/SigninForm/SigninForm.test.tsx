import React, { useRef } from "react";
import { render, screen, waitFor } from '@testing-library/react'
import userEvent from "@testing-library/user-event";
import SigninForm from '.'

describe('Form', () => {
    const focus = jest.fn();
    beforeEach(() => {
        jest.restoreAllMocks();
        jest.resetAllMocks();
    });

    const usernameMock = "user03";
    const passwordMock = "123456";
    const formIdMock = "Test"

    /**
     * This function is for setting form in form and check id.
     */
    it('should render the signin form', () => {
        const { container } = render(<SigninForm id={formIdMock} username={usernameMock} password={passwordMock}></SigninForm>)

        const form: HTMLFormElement | null = container.querySelector("#".concat(formIdMock));
        expect(form?.id).toBe(formIdMock);
    });

    /**
     * This function is for setting username in form and check value.
     */
    it('set username', () => {
        const { container } = render(<SigninForm name={formIdMock} username={usernameMock} password={passwordMock}></SigninForm>)

        const usernameInputScreen: HTMLInputElement | null = container.querySelector("#username");
        expect(usernameInputScreen?.value).toBe(usernameMock);
    });

    /**
     * This function is for setting password in form and check value.
     */
    it('set password', () => {
        const { container } = render(<SigninForm name={formIdMock} username={usernameMock} password={passwordMock}></SigninForm>)

        const passwordInputScreen: HTMLInputElement | null = container.querySelector("#password");
        expect(passwordInputScreen?.value).toBe(passwordMock);
    });
})
