import type { FormHTMLAttributes, FC, ReactNode } from 'react';

export interface ISigninFormProps extends FormHTMLAttributes<HTMLFormElement> {
  username: string;
  password: string;
}

export type SigninFormComponentType = FC<ISigninFormProps>;
export default SigninFormComponentType;
