import Router from "next/router";
import { setCookie, parseCookies } from 'nookies';
import { createContext, useEffect, useState, useMemo } from "react";

/**
 * The auth context.
 */
export const AuthContext = createContext({});

/**
 * The context provider.
 * @param {*} children - The all components children.
 * @returns The auth context provider.
 */
export function AuthProvider({ children }) {
    const [user, setUser] = useState(null);

    /**
     * This function is responsable for update the user state in refresh component. 
     */
    // useEffect(() => {
    //     const getSession = async () => {
    //         const response = await fetch("/api/user", {
    //             method: "GET",
    //         });
    //         response.json().then((json) => {
    //             if (json.user === "undefined") {
    //                 setUser(json.user);
    //             }
    //         });
    //     }

    //     getSession();
    // }, [user]);

    /**
     * Function is for call the login API.
     * @param {string} username - this personal username.
     * @param {string} password - this personal password.
     */
    const signIn = async (username, password) => {
        const response = await fetch("/api/login", {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({ username, password })
        });

        
        response.json().then((json) => {
            if (json.user) {
                setUser(json.user);
                Router.push("/private");
            }
        });

        // setCookie(undefined, 'MY_COOKIE', "token", {
        //     maxAge: 60 * 60 * 1, // 1 hour
        // });
    }

    return (
        <AuthContext.Provider value={{ user, setUser, signIn }}>
            {children}
        </AuthContext.Provider>
    )
}