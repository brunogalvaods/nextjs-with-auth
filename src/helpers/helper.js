import { withIronSessionSsr } from "iron-session/next";

export const getUserFromServerSession = ({ redirectToLogin }) => withIronSessionSsr(
    async ({ req, res }) => {
        try {
            const user = req.session.user;

            if (!user) throw new Error('unauthorized');
            
            return {
                props: {
                    // user: req.session.user
                }
            };
        } catch (e) {
            if (redirectToLogin) {
                return {
                    redirect: {
                        destination: "/signin", permanent: false
                    }
                };
            } else {
                return {
                    props: {},
                };
            }
        }
    },
    {
        cookieName: "MYSITECOOKIE",
        cookieOptions: {
            secure: process.env.NODE_ENV === "production" ? true : false
        },
        password: process.env.APPLICATION_SECRET
    }
);